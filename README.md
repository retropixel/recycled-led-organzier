[Recycled Jars Organizer] (http://link/)
=======================

This file describes the basic functionality of the Recycled Jars Organizer.  [link] (http://link/)


Description
===========
[Recycled Jars Organizer] (http://link/) is an organizing system using recyled parts (jars) to allow the storage of any components or parts. With the use of an electronic system we get visual feedback for easy location of the parts being organized.

Functionality principle
=======================

Each Jar contains an RGB led and a barcode. Each time you are looking for a specific item you can search in a database and the Jar where the item sits will light up. Also because you can take the jar with you you can later scan the barcode (trough a barcodescanner) and locate where the jar belongs to.  

Visual stimulation
==================

Of course the hole installation functions also as a lighting. It can be programmed to do crazy color animations like rainbows, stars, flash, and off course use it as a normal lighting.


Circuit
=======

The hardware connection can be seen in this picture:

![Schematic](https://bytebucket.org/retropixel/recycled-led-organzier/raw/401ca625fceb66d6c304e7ce4d62c76c6d1218ff/Media/schematic.jpg "Basic bloks of the installation")


Comunication
============

Comunication with arduino will be trough plain TCP/IP sockets (not websockets). Following pictures descrive the function of the protcol.
![Init_data] (https://bytebucket.org/retropixel/recycled-led-organzier/raw/886b726c9d74bfde82fc673717d80e67fbd483b8/Media/Socket_init_data_transfer.jpeg "Eschematic of data init")
![Led_data] (https://bytebucket.org/retropixel/recycled-led-organzier/raw/886b726c9d74bfde82fc673717d80e67fbd483b8/Media/Socket_RGB_data.jpeg "Eschematic of LED data")

Configuration
=============

----------

Use
===

----------