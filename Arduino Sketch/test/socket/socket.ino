#include <SPI.h>
#include <Ethernet.h>


///turn into server first for a better testing
// make it so it can change quickly from server to client

// Enter a MAC address for your controller below.
// Newer Ethernet shields have a MAC address printed on a sticker on the shield
byte mac[] = {  0x90, 0xA2, 0xDA, 0x00, 0xED, 0xC0 };

const int serverPort = 80;                // Server Port or client port to connect

//==== AS CLIENT ======================================================================
//IPAddress serverName(74,125,136,100); // Google
IPAddress serverName(192,168,1,15); // Google
// const char serverName[] = "drbit.nl";
const char GET_header[] = "/rjo/log_data.log";
//EthernetClient client;              // initialize the library instance as client
//====================================================================================

//====================================================================================
boolean force_static_ip = false;       // Focers Manual IP config, IP setings follow
IPAddress cust_ip(192,168,1,107);
IPAddress cust_dns(8,8,8,8);
IPAddress cust_gateway(192,168,1,1);
IPAddress cust_subnet(255,255,255,0);
//===================================================================================

//==== AS SERVER ====================================================================
EthernetServer server(serverPort);  // initialize the library instance as server

//===================================================================================
    

void setup() {
  Serial.begin(115200);
  Serial.println ("start");
  eth_get_ip();          // Get IP via DHCP or force static ip (force_static_ip = true)
  server.begin();
} 


int number_of_leds = 0;

void loop() {
  number_of_leds = get_LED_num ();
  get_LED_data (number_of_leds);
  //actAsClient ();       // Start as client (Normal operation)
}


void get_LED_data (int LEDn) {
  if (LEDn == 0) return;    // we dont have any led to count
  EthernetClient client = server.available ();
  byte recevie_case = 1;
  Serial.print ("Expecting number of LEDs DATA: ");
  Serial.println (LEDn);   // DEBUG
  int a = 1;
  while (true) {
    while (server.available() > 0) {      // wen server
      char x = client.read();
      if ((a == 1) && ((x == 13) || (x == 10))) {   // in case we have a double carriage (happens when testing from terminal)
        Serial.println ("skiping... CR NL");
        while (server.available() == 0) {}    // Wait until we receive a new character
        x = client.read();
      }
      //Serial.print (x);   // DEBUG
      switch (recevie_case) {
        case 1:{ // waiting for init
          if (x == '!') {
            recevie_case = 2;   // start
            Serial.println ("received -> !");
            client.write("1");
            Serial.print ("Led Num: ");
            Serial.print (a);   // DEBUG
            a++;    // we flag a new led counter
          }else{
            Serial.println ("wrong sentence\n");
            client.write("0");
            // end LED data
            return;
          }
          break;
        }
        case 2:{   // start receiving first char
          // receive Higher byte
          // Send R data to leds
          Serial.print (F(" Red = "));
          Serial.print (x, DEC);   // DEBUG
          recevie_case = 3;
          break;
        }
        case 3:{  // start receiving second char
          // Send G data to leds
          Serial.print (F(" Green = "));
          Serial.print (x, DEC);   // DEBUG
          recevie_case = 4;
          break;
        }
        case 4:{  // start receiving second char
          // Send B data to leds
          Serial.print (F(" Blue = "));
          Serial.println (x, DEC);   // DEBUG
          recevie_case = 1;
          if (a == LEDn) {
            Serial.println ("Done!");
            client.write("1");
            return;
          }
          break;
        }
        default:{   // not needed but in case something goes wrong
          // if nothing else matches, do the default
          Serial.println ("default (state not recognized)");
        }
      }
    }
  }
}


int get_LED_num () {
  char first_char = 0;
  char second_char = 0;
  byte recevie_case = 1;

  EthernetClient client = server.available ();

  while (true) {
    while (server.available() > 0) {      // wen server
      if (!client.connected()) return 0;    // Allows to quit if client is disconnected
      char x = client.read();
      Serial.print (x);   // DEBUG
      switch (recevie_case) {
        case 1:{ // waiting for init
          if (x == '!') {
            recevie_case = 2;   // start
            Serial.println ("received -> !");
            client.write("1");
          }
          break;
        }
        case 2:{   // start receiving first char
          // receive Higher byte
          first_char = x;
          Serial.print ("fisrt char: ");
          Serial.print (first_char);
          Serial.print (" - ");
          Serial.println (first_char, BIN);
          recevie_case = 3;
          break;
        }
        case 3:{  // start receiving second char
          second_char = x;
          Serial.print ("second char: ");
          Serial.print (second_char);
          Serial.print (" - ");
          Serial.println (second_char, BIN);
          recevie_case = 4;
          break;
        }
        case 4:{ // waiting for init
          if (x == '!') {
            recevie_case = 5;   // start
            Serial.println ("received ending -> !");
          }else{
            Serial.println ("wrong sentence\n");
            recevie_case = 1;
            client.write("0");
          }
          break;
        }
        case 5:{   // start receiving first char
          if ((x == 13) || (x == 10)) {   // begining or end of command
            int number = first_char << 8;
            number = number | second_char;
            Serial.print ("final number: ");
            Serial.print (number, DEC);
            Serial.print (" - ");
            Serial.println (number, BIN);
            client.write("1");
            return number;
            // Do nothing..
          }else {
            Serial.println ("wrong sentence\n");
            recevie_case = 1;
            client.write("0");
          }
          break;
        }
        default:{   // not needed but in case something goes wrong
          // if nothing else matches, do the default
          Serial.println ("default (state not recognized)");
        }
      }
    }  
  } 
}




/*      /// modified look for the original
void Client_data () {
  char first_char = 0;
  char second_char = 0;
  boolean all_ok = false;
  int recevie_case = 1;

  while (!all_ok) {
    // while (client.available() > 0) {   // wen client
    while (server.available() > 0) {      // wen server
      Serial.println ("in all ok");
      if (!client.connected()) return;    // Allows to quit if client is disconnected
      char x = client.read();
      Serial.print (x);   // DEBUG
      switch (recevie_case) {
        case 1:{ // waiting for init
          if (x == '!') recevie_case = 2;   // start
          Serial.println ("received!");
          break;
        }
        case 2:{   // start receiving first char
          // receive Higher byte
          first_char = x;
          Serial.print ("fisrt char: ");
          Serial.print (first_char);
          Serial.print (" - ");
          Serial.println (first_char, BIN);
          recevie_case = 3;
          break;
        }
        case 3:{  // start receiving second char
          second_char = x;
          Serial.print ("second char: ");
          Serial.print (second_char);
          Serial.print (" - ");
          Serial.println (second_char, BIN);
          recevie_case = 4;
          break;
        }
        case 4:{   // start receiving first char
          if ((x == 13) || (x == 10)) {   // begining or end of command
            all_ok = true;
            int number = first_char << 8;
            number = number | second_char;
            Serial.print ("final number: ");
            Serial.print (number, DEC);
            Serial.print (" - ");
            Serial.println (number, BIN);
            recevie_case = 1;
            // Do nothing..
          }else {
            all_ok = false;
            Serial.println ("wrong sentence\n");
            //error
            //dicard
            recevie_case = 1;
          }
        }
        default:{
          // if nothing else matches, do the default
          Serial.println ("default");
        }
      }
    }  
  } 
}

*/


void eth_get_ip() {
  Select_ETH ();        // Select ethernet to be active

  // start the Ethernet connection using a DHCP or a fixed IP address and DNS server:
  Serial.println(F("Getting IP"));
  if (force_static_ip) {
    Serial.println(F("Forcing static IP"));
    force_static_IP ();
  }else if (!Ethernet.begin(mac)) {   // If we can not get DHCP then force
    Serial.println(F("DHCP Failed, setting static IP"));
    force_static_IP ();
  }
  print_IP_config ();   // Prints config. 
  //lastConnectionTime = millis() - postingInterval;  // Simulates last connection
}

void force_static_IP () {
  Ethernet.begin(mac, cust_ip, cust_dns, cust_gateway, cust_subnet);
}

void print_IP_config () {     // print the Ethernet board/shield's IP address:
  Serial.print(F("IP: "));
  Serial.println(Ethernet.localIP());
  Serial.print(F("Gateway: "));
  Serial.println(Ethernet.gatewayIP());
  Serial.print(F("DNS: "));
  Serial.println(Ethernet.dnsServerIP());
  Serial.print(F("Subnet: "));
  Serial.println(Ethernet.subnetMask());
  Serial.print(F("Port: "));
  Serial.println(serverPort);
}

//-----------------------------------------------------------------------
// functions to select SD and Ethernet
#define SS_SD_CARD   4 
#define SS_ETHERNET 10

void Select_ETH () {
  digitalWrite(SS_SD_CARD, HIGH);  // SD Card not active
  digitalWrite(SS_ETHERNET, LOW);  // Ethernet ACTIVE
}

void Select_SD () {
  digitalWrite(SS_ETHERNET, HIGH); // Ethernet not active
  digitalWrite(SS_SD_CARD, LOW);   // SD Card ACTIVE
}

/*

void loop() {
  // listen for incoming clients
  
  if (client) {
    Serial.println("new client");
    // an http request ends with a blank line
    boolean currentLineIsBlank = true;
    while (client.connected()) {
      if (client.available()) {
        char c = client.read();
        Serial.write(c);
        // if you've gotten to the end of the line (received a newline
        // character) and the line is blank, the http request has ended,
        // so you can send a reply
        if (c == '\n' && currentLineIsBlank) {
          // send a standard http response header
          client.println("HTTP/1.1 200 OK");
          client.println("Content-Type: text/html");
          client.println("Connection: close");  // the connection will be closed after completion of the response
    client.println("Refresh: 5");  // refresh the page automatically every 5 sec
          client.println();
          client.println("<!DOCTYPE HTML>");
          client.println("<html>");
          // output the value of each analog input pin
          for (int analogChannel = 0; analogChannel < 6; analogChannel++) {
            int sensorReading = analogRead(analogChannel);
            client.print("analog input ");
            client.print(analogChannel);
            client.print(" is ");
            client.print(sensorReading);
            client.println("<br />");       
          }
          client.println("</html>");
          break;
        }
        if (c == '\n') {
          // you're starting a new line
          currentLineIsBlank = true;
        } 
        else if (c != '\r') {
          // you've gotten a character on the current line
          currentLineIsBlank = false;
        }
      }
    }
    // give the web browser time to receive the data
    delay(1);
    // close the connection:
    client.stop();
    Serial.println("client disonnected");
  }
}

*/

/*
#include <Servo.h>
char messageBuffer[12], cmd[3], pin[3], val[4], aux[4];
boolean debug = false;
int index = 0;
Servo servo;

void setup() {
  Serial.begin(115200);
}

void loop() {
  while(Serial.available() > 0) {
    char x = Serial.read();
    if (x == '!') {   // start
      char char1 = Serial.read();
      char char2 = Serial.read();
      //check that we have a carriage return and NL  and continue

    }      
    else if (x == '.') process(); // end
    else messageBuffer[index++] = x;
  }
}


// Funtion that returns a number typed in the serial interface
int get_number(int buffer) {
  buffer = buffer +1;
  char PositionN[buffer];
  int length = 0;
  boolean char_received = false;
  boolean end_char = false;
  // Accept NL CR
  while (!char_received || !end_char) {
    if (Serial.available () >0) {
      char c = Serial.read();
      if ((c == 13) || (c == 10)) {   // begining or end of command
        end_char = true;
        // Do nothing..
      }else {
        PositionN[length] = c;
        length = (length+1) % buffer;
        char_received = true;
        end_char = false;
      }
      delay(30);
    }
  }
  PositionN[length] = '\0';
  
  // Staring of script
  // String SpositionN = PositionN;
  int num = 0;
  for (int i = (buffer-2); i>=0 ; i--) {
    num = atoi(&PositionN[i]);
  }
  //Serial.println (num);
  return (int) num;
}

// Deal with a full message and determine function to call
 
void process() {
  index = 0;
  
  strncpy(cmd, messageBuffer, 2);
  cmd[2] = '\0';
  strncpy(pin, messageBuffer + 2, 2);
  pin[2] = '\0';
  strncpy(val, messageBuffer + 4, 3);
  val[3] = '\0';
  strncpy(aux, messageBuffer + 7, 3);
  aux[3] = '\0';
  
  if (debug) {
    Serial.println(messageBuffer); }

  int cmdid = atoi(cmd);
  
  switch(cmdid) {
    case 0:  sm(pin,val);               break;
    case 1:  dw(pin,val);               break;
    case 2:  dr(pin);                   break;
    case 3:  aw(pin,val);               break;
    case 4:  ar(pin);                   break;
    case 90: autoReply();               break;
    case 98: handleServo(pin,val,aux);  break;
    case 99: toggleDebug(val);          break;
    default:                            break;
  }
}

 // @param char val value for enabling or disabling debugger (0 = false, 1 = true)
 
void toggleDebug(char *val) {
  if (atoi(val) == 0) {
    debug = false;
    Serial.println("goodbye");
  } else {
    debug = true;
    Serial.println("hello");
  }
}

void autoReply() {
   Serial.println('Is Dave there?'); 
}


 // Set pin mode
 // @param char pin identifier for pin
 // @param char val set pit to OUTPUT or INPUT

void sm(char *pin, char *val) {
  if (debug) {
    Serial.println("sm"); }
    
  int p = getPin(pin);
  if (p == -1 && debug) {
    Serial.println("badpin"); 
  } else {  
    if (atoi(val) == 0) {
      pinMode(p, OUTPUT);
    } else {
      pinMode(p, INPUT);
    }
  }
}


 // Digital write
 // @param char pin identifier for pin
 // @param char val set pin to HIGH or LOW
 
void dw(char *pin, char *val) {
  if (debug) {
    Serial.println("dw"); }
    
  int p = getPin(pin);
  if (p == -1 && debug) {
    Serial.println("badpin"); 
  } else {  
    pinMode(p, OUTPUT);
    if (atoi(val) == 0) {
      digitalWrite(p, LOW);
    } else {
      digitalWrite(p, HIGH);
    }
  }
}


 // Digital read
 // @param char pin pin identifier

void dr(char *pin) {
  if (debug) {
    Serial.println("dr"); }
    
  int p = getPin(pin);
  if (p == -1 && debug) {
    Serial.println("badpin"); 
  } else {
    pinMode(p, INPUT);
    int oraw = digitalRead(p);
    char m[7];
    sprintf(m, "%02d::%02d", p,oraw);
    Serial.println(m);
  }
}


 // Analog read
 // @param char pin pin identifier

void ar(char *pin) {
  if (debug) {
    Serial.println("ar"); }

  int p = getPin(pin);
  if (p == -1 && debug) {
    Serial.println("badpin"); 
  } else {
    pinMode(p, INPUT); // don't want to sw
    int rval = analogRead(p);
    char m[8];
    sprintf(m, "%s::%03d", pin, rval);
    Serial.println(m);
  }
}  


 // Analog write
 //@param char pin pin identifier

void aw(char *pin, char *val) {
  if (debug) {
    Serial.println("aw"); }
    
  int p = getPin(pin);
  if (p == -1 && debug) {
    Serial.println("badpin"); 
  } else {
    pinMode(p, OUTPUT);
    analogWrite(p, atoi(val));
  }
}

int getPin(char *pin) { //Converts to A0-A5, and returns -1 on error
  int ret = -1;
  if (pin[0] == 'A' || pin[0] == 'a') {
    switch(pin[1]) {
      case '0': ret = A0; break;
      case '1': ret = A1; break;
      case '2': ret = A2; break;
      case '3': ret = A3; break;
      case '4': ret = A4; break;
      case '5': ret = A5; break;
      default:            break;
    }
  } else {
    ret = atoi(pin);
    if (ret == 0 && (pin[0] != '0' || pin[1] != '0')) {
      ret = -1; }
  }
  
  return ret;
}
  


 // Handle Servo commands
 // attach, detach, write, read, writeMicroseconds, attached

void handleServo(char *pin, char *val, char *aux) {
  if (debug) {
    Serial.println("ss"); }
    
  int p = getPin(pin);
  if (p == -1 && debug) {
    Serial.println("badpin"); 
  } else {
    Serial.println("got signal");
    if (atoi(val) == 0) {
      servo.detach();
    } else if (atoi(val) == 1) {
      servo.attach(p);
      Serial.println("attached");
    } else if (atoi(val) == 2) {
      Serial.println("writing to servo");
      Serial.println(atoi(aux));
      servo.write(atoi(aux));
    }  
  }
}

*/