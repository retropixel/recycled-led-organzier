/*
  Web client
 
 This sketch connects to a website (http://www.google.com)
 using an Arduino Wiznet Ethernet shield. 
 
 Circuit:
 * Ethernet shield attached to pins 10, 11, 12, 13
 
 created 18 Dec 2009
 modified 9 Apr 2012
 by David A. Mellis
 
 */

#include <SPI.h>
#include <Ethernet.h>

// Enter a MAC address for your controller below.
// Newer Ethernet shields have a MAC address printed on a sticker on the shield
byte mac[] = {  0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xDF };
IPAddress server(74,125,136,100); // Google
char serverName[] = "www.google.com";
IPAddress cust_ip(192,168,1,107);
IPAddress cust_dns(8,8,8,8);
IPAddress cust_gateway(192,168,1,1);
IPAddress cust_subnet(255,255,255,0);

// Initialize the Ethernet client library
// with the IP address and port of the server 
// that you want to connect to (port 80 is default for HTTP):
EthernetClient client;

void setup() {
  Select_ETH ();  // Select Ethernet to be active
  // Open serial communications and wait for port to open:
  Serial.begin(9600);
   while (!Serial) {
    ; // wait for serial port to connect. Needed for Leonardo only
  }

    Serial.println(F("Getting IP through DHCP..."));
  if (!Ethernet.begin(mac)) {
    Serial.println(F("Failed to configure Ethernet using DHCP, setting static IP..."));
    Ethernet.begin(mac, cust_ip, cust_dns, cust_gateway, cust_subnet); 
  }
  delay(500);
  print_IP_config ();   // Prints config.

  // give the Ethernet shield a second to initialize:
  delay(1000);
  Serial.println("connecting...");

  // if you get a connection, report back via serial:
  //if (client.connect(server, 80)) {       // Using IPAddress
  if (client.connect(serverName, 80)) {     // Using Name (only when we have a DNS)
    Serial.println("connected");
    // Make a HTTP request:
    client.println("GET /search?q=arduino HTTP/1.0");
    client.print("Host: ");
    client.println (serverName);
    client.println("User-Agent: arduino-ethernet");
    client.println("Connection: keep-alive");
    client.println();
  } 
  else {
    // kf you didn't get a connection to the server:
    Serial.println("connection failed");
  }
}

void loop()
{
  // if there are incoming bytes available 
  // from the server, read them and print them:
  if (client.available()) {
    char c = client.read();
    Serial.print(c);
  }

  // if the server's disconnected, stop the client:
  if (!client.connected()) {
    Serial.println();
    Serial.println("disconnecting.");
    client.stop();

    // do nothing forevermore:
    for(;;)
      ;
  }
}

void print_IP_config () {
  // print the Ethernet board/shield's IP address:
  Serial.print("My IP address: ");
  Serial.println(Ethernet.localIP());
  Serial.print("My gateway address: ");
  Serial.println(Ethernet.gatewayIP());
  Serial.print("My DNS address: ");
  Serial.println(Ethernet.dnsServerIP());
  Serial.print("My Subnet mask: ");
  Serial.println(Ethernet.subnetMask());
}

#define SS_SD_CARD   4 
#define SS_ETHERNET 10

void Select_ETH () {
  digitalWrite(SS_SD_CARD, HIGH);  // SD Card not active
  digitalWrite(SS_ETHERNET, LOW);  // Ethernet ACTIVE
}

void Select_SD () {
  digitalWrite(SS_ETHERNET, HIGH); // Ethernet not active
  digitalWrite(SS_SD_CARD, LOW);  // SD Card ACTIVE
}



