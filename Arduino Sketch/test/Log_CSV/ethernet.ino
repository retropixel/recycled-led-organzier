/*
 This sketch is a modification of "Repeating Web client" (arduino exmaple) - http://arduino.cc/en/Tutorial/WebClientRepeating

 It is modified to be able to show basic infor from the DHCP and an option to quickly
 switch to manual config.

 It aslo fixes a problem with the ehternet module not working when having an SD card inserted
 
 This sketch connects to a a web server and reads as plain text.
 using a Wiznet Ethernet shield. You can use the Arduino Ethernet shield, or
 the Adafruit Ethernet shield, either one will work, as long as it's got
 a Wiznet Ethernet module on board.
 
 This example uses DNS, by assigning the Ethernet client with a MAC address,
 IP address, and DNS address.
 
 Circuit:
 * Ethernet shield attached to pins 10, 11, 12, 13
 
 Modified by Doctor Bit (www.drbit.nl) 13/08/2014
 created 19 Apr 2012
 by Tom Igoe
 
 This code is in the public domain.
 
 */

// Enter a MAC address for your controller below.
// Newer Ethernet shields have a MAC address printed on a sticker on the shield
byte mac[] = {  0x90, 0xA2, 0xDA, 0x00, 0xED, 0xC0 };
//IPAddress server(74,125,136,100); // Google
const char serverName[] = "drbit.nl";
const char GET_header[] = "/rjo/log_data.log";
int serverPort = 80;                // Server Port to connect
IPAddress cust_ip(192,168,1,107);
IPAddress cust_dns(8,8,8,8);
IPAddress cust_gateway(192,168,1,1);
IPAddress cust_subnet(255,255,255,0);
boolean force_manual = false;       // Focers Manual IP config

unsigned long lastConnectionTime = 0;          // last time you connected to the server, in milliseconds
boolean lastConnected = false;                 // state of the connection last time through the main loop
const unsigned long postingInterval = 20*1000;  // delay between updates, in milliseconds

boolean rFlag = false;
boolean nFlag = false;
boolean dataStart = false;

void init_eth() {
  Select_ETH ();        // Select ethernet to be active

  // start the Ethernet connection using a DHCP or a fixed IP address and DNS server:
  Serial.println(F("Getting IP"));
  if (force_manual) {
    Serial.println(F("Forcing static IP"));
    Ethernet.begin(mac, cust_ip, cust_dns, cust_gateway, cust_subnet);
  }else if (!Ethernet.begin(mac)) {
    Serial.println(F("DHCP Failed, setting static IP"));
    Ethernet.begin(mac, cust_ip, cust_dns, cust_gateway, cust_subnet);
  }
  //print_IP_config ();   // Prints config. 
  lastConnectionTime = millis() - postingInterval;  // Simulates last connection

}

void retireve_server_data() {
  Select_ETH ();        // Select ethernet to be active
  //Serial.println("start loop");
  // if there's incoming data from the net connection.
  // send it out the serial port.  This is for debugging
  // purposes only:
  if (client.available()) {
    if (!dataStart) {   // Receiveing Http header
      char c = client.read();
      Serial.print(c);
      if (data_Start (&c)) {
        dataStart = true;
        //Serial.print(F("\n\r*Start Recording\n\r"));
        // Take into account that the first /n or /r will scape and be printed in the file before the real data starts
      }
    }else{    // Data in here can be recorded in the SD
      char c = client.read();     // Read data from ethernet
      Serial.print(c);            // Print out to the serial
      write_char_into_file (&c);  // Write character into the SD file
    }
  }

  // if there's no net connection, but there was one last time
  // through the loop, then stop the client:
  if (!client.connected() && lastConnected) {
    Serial.println();
    Mem_report ();
    Serial.println(F("disconnecting."));
    client.stop();
    clearDataFlags ();
    sync_and_close_file ();
    lastConnected = client.connected();     // Record timing reference for next request
  }

  // if you're not connected, and ten seconds have passed since
  // your last connection, then connect again and send data:
  if(!client.connected() && ((millis()-lastConnectionTime) > postingInterval)) {
  //if(!client.connected()) {
    httpRequest();    // Creates a connection and sends http request.
  }

  lastConnected = client.connected();     // Record timing reference for next request
}

// this method makes a HTTP connection to the server:
void httpRequest() {
  
  Serial.print(F("\nconnecting to: "));    // Debug info
  Serial.print(serverName);
  Serial.print(F(":"));
  Serial.print(serverPort);
  Serial.print(F(":"));
  Serial.println(GET_header);
  Mem_report ();

  if (client.connect(serverName, serverPort)) {   // if there's a successful connection:
    Serial.println(F("connected!"));
    // send the HTTP PUT request:
    client.print(F("GET "));
    client.print (GET_header);
    client.print(F("?"));   // We add ?rand.number at the end to avoid cashin of data by mid servers
    client.print (random(100));
    client.println(F(" HTTP/1.0"));
    client.print(F("Host: "));
    client.println(serverName);
    //client.println(F("User-Agent: arduino-ethernet"));
    client.println(F("Connection: close"));
    client.println();
    // note the time that the connection was made:
    lastConnectionTime = millis();
  } else {    // if you couldn't make a connection:
    Serial.println(F("connection failed"));
    //Serial.println("disconnecting.");
    client.stop();
  }
}

boolean data_Start (char *c) {
  // We check if we receive 2 times a carriage return or newline
  // if this is true means that the header finished and we begin with the data
  if (*c == '\r') {
    if (!rFlag) {
      rFlag = true;
    }else {
      return true;
    }
  }else if ((*c != '\r') && (*c != '\n')) {    // if its a different caracter than carriage or newline
    rFlag = false;
  }

  if (*c == '\n') {
    if (!nFlag) {
      nFlag = true;
    }else {
      return true;
    }
  }else if ((*c != '\r') && (*c != '\n')){     // if its a different caracter than carriage or newline
    nFlag = false;
  }

  return false;
}

void clearDataFlags () {
  rFlag = false;
  nFlag = false;
  dataStart = false;
}

void print_IP_config () {     // print the Ethernet board/shield's IP address:
  Serial.print(F("IP: "));
  Serial.println(Ethernet.localIP());
  Serial.print(F("Gateway: "));
  Serial.println(Ethernet.gatewayIP());
  Serial.print(F("DNS: "));
  Serial.println(Ethernet.dnsServerIP());
  Serial.print(F("Subnet: "));
  Serial.println(Ethernet.subnetMask());
}