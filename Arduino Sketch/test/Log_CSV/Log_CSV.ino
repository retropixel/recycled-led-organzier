#include <SPI.h>
#include <Ethernet.h>
#include <SdFat.h>
#include <Adafruit_NeoPixel.h>

SdFat sd;             // File system object.
SdFile file;          // Use for file creation in folders.
ArduinoOutStream cout(Serial);      // Create a Serial output stream.
char cinBuf[40];      // Buffer for Serial input.
ArduinoInStream cin(Serial, cinBuf, sizeof(cinBuf));    // Create a serial input stream.

EthernetClient client;// initialize the library instance:

void setup() {
  Serial.begin(9600);
  while (!Serial) {}  // wait for Leonardo
  delay(1000);
  
  cout << pstr("Type a character to start\n");    // Wait for input line and discard.
  cin.readline();
  init_eth();         // Init ethernet module and DHCP
  init_sd();          // Init SD card
  findFileName ();    // Backup old file and create a new one
  Mem_report ();      // Prints free memory on serial interface
  get_latest_DB_file (); // Gets data from the server and stores it in the SD card

}

//------------------------------------------------------------------------------
void loop() {
  retireve_server_data();
  if (Serial.available ()) {
    cin.readline();   // read serial
    readFile();
    Mem_report ();
  }
}

void dataArrived(WebSocketClient client, String data) {
  Serial.println("Data Arrived: " + data);
}


//-----------------------------------------------------------------------
// functions to select SD and Ethernet
#define SS_SD_CARD   4 
#define SS_ETHERNET 10

void Select_ETH () {
  digitalWrite(SS_SD_CARD, HIGH);  // SD Card not active
  digitalWrite(SS_ETHERNET, LOW);  // Ethernet ACTIVE
}

void Select_SD () {
  digitalWrite(SS_ETHERNET, HIGH); // Ethernet not active
  digitalWrite(SS_SD_CARD, LOW);   // SD Card ACTIVE
}