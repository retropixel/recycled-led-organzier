// SD card chip select pin.
const uint8_t SD_CHIP_SELECT = 4;

//==============================================================================
// Error messages stored in flash.
#define error(msg) error_P(PSTR(msg))
//------------------------------------------------------------------------------
void error_P(const char* msg) {
  sd.errorHalt_P(msg);
}
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// #define FILE_BASE_NAME "RJOMAP"

void findFileName () {
  Select_SD ();
  const char directory[7] = "RJO_DB";
  const char fileName[11] = "RJOMAP.CSV";
  const char BAKfileName[11] = "RJOMAP.BAK";
  
  if (!sd.exists(directory)) {        // If the folder does not exist we create it
    sd.mkdir(directory);         // Create a new folder.
  }
  sd.chdir(directory);   // Change current directory.

  if (sd.exists(fileName)) {
    if (sd.exists(BAKfileName)) {
      if (!sd.remove(BAKfileName)) error("remove failed");    // delete BAK file
    }
    // Now that we have the next name available we can rename the old into the new
    if (!sd.rename(fileName,BAKfileName)) error("rename bak");
    cout << pstr("Rename BAK succsess") << endl;
  }

  create_file (fileName);    // Create file
  if (!sd.chdir()) error("chdir failed.\n");      // Change current directory to root.
  // test_writing_into_file ("RJO_DB/RJOMAP.CSV","Message");     // As a test write someting in the file
  // list_files ();        // List all files

}

void init_sd () {
  Select_SD ();
  // Initialize the SD card at SPI_HALF_SPEED to avoid bus errors with
  // breadboards.  use SPI_FULL_SPEED for better performance.
  if (!sd.begin(SS_SD_CARD, SPI_HALF_SPEED)) sd.initErrorHalt();
}

void create_file (const char* _file) {
  if (!file.open(_file, O_CREAT | O_WRITE))error("creating file");
  file.close();
}

void test_writing_into_file (char* _file, char* msg) {
  // Testing to write into the file
  if (!file.open(_file, O_WRITE | O_APPEND))error("opening file");
  file.println(msg);
  file.close();
}

void list_files () {
  cout << pstr("List of files on the SD.\n");
  sd.ls(LS_R);
}

void write_char_into_file (char *c) {
	Select_SD ();        // Select SD to be active
	if (!file.isOpen  ()) {   // If the file is not opened we open it
		if (!file.open("RJO_DB/RJOMAP.CSV", O_WRITE | O_APPEND)) error("file.open");  // Open file
	}
	file.print(c);        // Print C into the file   // file.write(',');   // Doing file write if it letters
	Select_ETH ();        // Select ethernet to be active
}

void sync_and_close_file () {		// Close File
    Select_SD ();        	// Select SD to be active
    file.print(F("\r\n"));   	// Prints new return and new line in case we want to append new things
    if (!file.sync() || file.getWriteError()) error("write");	// Force data to SD and update the directory entry to avoid data loss.
    file.close();   		// Close file
    Select_ETH ();        	// Select ethernet to be active
}

char fileToRead[18] = "RJO_DB/RJOMAP.CSV";

void readFile() {
  Select_SD ();        // Select SD to be active

  // re-open the file for reading:
  if (!file.open(fileToRead, O_READ)) {
  sd.errorHalt("opening for read failed");
  }
  Serial.println(fileToRead);

  // read from the file until there's nothing else in it:
  int data;
  while ((data = file.read()) >= 0) Serial.write(data);
  // close the file:
  file.close();
}

/*
 *  This example reads a simple CSV, comma-separated values, file.
 *  Each line of the file has three values, a long and two floats.


void readCSV() {
  long lg;
  float f1, f2;
  char text[10];
  char c1, c2, c3;  // space for commas.
  
  // open input file
  ifstream sdin("RJO_DB/RJOMAP.CSV");
  
  // check for open error
  if (!sdin.is_open()) error("open");
  
  // read until input fails
  while (1) {
    // Get text field.
    sdin.get(text, sizeof(text), ',');
    
    // Assume EOF if fail.
    if (sdin.fail()) break;
    
    // Get commas and numbers.
    sdin >> c1 >> lg >> c2 >> f1 >> c3 >> f2;
    
    // Skip CR/LF.
    sdin.skipWhite();
    
    if (sdin.fail()) error("bad input");
    
    // error in line if not commas
    if (c1 != ',' || c2 != ',' || c3 != ',') error("comma");
    
    // print in six character wide columns
    cout << text << setw(6) << lg << setw(6) << f1 << setw(6) << f2 << endl;
  }
  // Error in an input line if file is not at EOF.
  if (!sdin.eof()) error("readFile");
}
//------------------------------------------------------------------------------
// write test file
void writeCSV() {

  // create or open and truncate output file
  ofstream sdout("example.CSV");
  
  // write file from string stored in flash
  sdout << pstr(
    "Line 1,1,2.3,4.5\n"
    "Line 2,6,7.8,9.0\n"
    "Line 32,9,8.7,6.5\n"
    "Line 4,-4,-3.2,-1\n") << flush;

  // check for any errors
  if (!sdout) error("writeFile");
  
  sdout.close();
}
 */

/*
 SD card read/write
  
 This example shows how to read and write data to and from an SD card file 	
 The circuit:
 * SD card attached to SPI bus as follows:
 ** MOSI - pin 11
 ** MISO - pin 12
 ** CLK - pin 13
 ** CS - pin 4
 
 created   Nov 2010
 by David A. Mellis
 updated 2 Dec 2010
 by Tom Igoe
 modified by Bill Greiman 11 Apr 2011
 This example code is in the public domain.
 	 
*/



/*

// Log file base name.  Must be six characters or less.
#define FILE_BASE_NAME "DATA"
//------------------------------------------------------------------------------

void writeHeader() {
  file.print(F("micros"));
  for (uint8_t i = 0; i < ANALOG_COUNT; i++) {
    file.print(F(",adc"));
    file.print(i, DEC);
  }
  file.println();
}
//------------------------------------------------------------------------------
// Log a data record.
void logData() {
  uint16_t data[ANALOG_COUNT];
  
  // Read all channels to avoid SD write latency between readings.
  for (uint8_t i = 0; i < ANALOG_COUNT; i++) {
    data[i] = analogRead(i);
  }
  // Write data to file.  Start with log time in micros.
  file.print(logTime);
  
  // Write ADC data to CSV record.
  for (uint8_t i = 0; i < ANALOG_COUNT; i++) {
    file.write(',');
    file.print(data[i]);
  }
  file.println();
}

//------------------------------------------------------------------------------
void setup() {
  Select_SD ();  // Disable enet
  const uint8_t BASE_NAME_SIZE = sizeof(FILE_BASE_NAME) - 1;
  char fileName[13] = FILE_BASE_NAME "00.CSV";

  if (BASE_NAME_SIZE > 6) {
    error("FILE_BASE_NAME too long");
  }
  while (sd.exists(fileName)) {
    if (fileName[BASE_NAME_SIZE + 1] != '9') {
      fileName[BASE_NAME_SIZE + 1]++;
    } else if (fileName[BASE_NAME_SIZE] != '9') {
      fileName[BASE_NAME_SIZE + 1] = '0';
      fileName[BASE_NAME_SIZE]++;
    } else {
      error("Can't create file name");
    }
  }
  if (!file.open(fileName, O_CREAT | O_WRITE | O_EXCL)) error("file.open");
  do {
    delay(10);
  } while (Serial.read() >= 0);
  
  Serial.print(F("Logging to: "));
  Serial.println(fileName);
  Serial.println(F("Type any character to stop"));
  
  // Write data header.
  writeHeader();
  
}
//------------------------------------------------------------------------------
void loop() {
  
  logData();
  // Force data to SD and update the directory entry to avoid data loss.
  if (!file.sync() || file.getWriteError()) error("write error"); 
}

*/



  /*
  // Create a new folder.
  if (!sd.mkdir("FOLDER1")) error("Create FOLDER1 failed");
  cout << pstr("Created FOLDER1\n");
  
  // Create a file in FOLDER1 using a path.
  if (!file.open("FOLDER1/FILE1.TXT", O_CREAT | O_WRITE)) {
    error("create FOLDER1/FILE1.TXT failed");
  }
  file.close();
  cout << pstr("Created FOLDER1/FILE1.TXT\n");
  
  // Change volume working directory to FOLDER1.
  if (!sd.chdir("FOLDER1")) error("chdir failed for FOLDER1.\n");
  cout << pstr("chdir to FOLDER1\n");
  
  // Create FILE2.TXT in current directory.
  if (!file.open("FILE2.TXT", O_CREAT | O_WRITE)) {
    error("create FILE2.TXT failed");
  }
  file.close();
  cout << pstr("Created FILE2.TXT in current directory\n");
  
  cout << pstr("List of files on the SD.\n");
  sd.ls("/", LS_R);

  // Remove files from current directory.
  if (!sd.remove("FILE1.TXT") || !sd.remove("FILE2.TXT")) error("remove failed");
  cout << pstr("\nFILE1.TXT and FILE2.TXT removed.\n");

  // Change current directory to root.
  if (!sd.chdir()) error("chdir to root failed.\n");
  
  cout << pstr("List of files on the SD.\n");
  sd.ls(LS_R);
  
  // Remove FOLDER1.
  if (!sd.rmdir("FOLDER1")) error("rmdir for FOLDER1 failed\n");
  
  cout << pstr("\nFOLDER1 removed, SD empty.\n");
  cout << pstr("Done!\n");

  //--------------------------------------------------------------------------------

  // create a file and write one line to the file
  file("NAME1.TXT", O_WRITE | O_CREAT);
  if (!file.isOpen()) error("NAME1");
  file.println("A test line for NAME1.TXT");

  // rename the file NAME2.TXT and add a line.
  // sd.vwd() is the volume working directory, root.
  if (!file.rename(sd.vwd(), "NAME2.TXT")) error("NAME2");
  file.println("A test line for NAME2.TXT");

  // list files
  cout << pstr("------") << endl;
  sd.ls(LS_R);

  // make a new directory - "DIR1"
  if (!sd.mkdir("DIR1")) error("DIR1");

  // move file into DIR1, rename it NAME3.TXT and add a line
  if (!file.rename(sd.vwd(), "DIR1/NAME3.TXT")) error("NAME3");
  file.println("A line for DIR1/NAME3.TXT");

  // list files
  cout << pstr("------") << endl;
  sd.ls(LS_R);

  // make directory "DIR2"
  if (!sd.mkdir("DIR2")) error("DIR2");

  // close file before rename(oldPath, newPath)
  file.close();

  // move DIR1 into DIR2 and rename it DIR3
  if (!sd.rename("DIR1", "DIR2/DIR3")) error("DIR2/DIR3");

  // open file for append in new location and add a line
  if (!file.open("DIR2/DIR3/NAME3.TXT", O_WRITE | O_APPEND)) {
    error("DIR2/DIR3/NAME3.TXT");
  }
  file.println("A line for DIR2/DIR3/NAME3.TXT");

  // list files
  cout << pstr("------") << endl;
  sd.ls(LS_R);

  cout << pstr("Done") << endl;
  */



  /*

//------------------------------------------------------------------------------
// Permit SD to be wiped if ALLOW_WIPE is true.
const bool ALLOW_WIPE = false;

  // Check for empty SD. Format SD!!!!!!!!!!
  if (file.openNext(sd.vwd(), O_READ)) {
    cout << pstr("Found files/folders in the root directory.\n");    
    if (!ALLOW_WIPE) {
      error("SD not empty, use a blank SD or set ALLOW_WIPE true.");  
    } else {
      cout << pstr("Type: 'WIPE' to delete all SD files.\n");
      char buf[10];
      cin.readline();
      cin.get(buf, sizeof(buf));
      if (cin.fail() || strncmp(buf, "WIPE", 4) || buf[4] >= ' ') {
        error("Invalid WIPE input");
      }
      file.close();
      sd.vwd()->rmRfStar();
      cout << pstr("***SD wiped clean.***\n\n");
    }
  }*/